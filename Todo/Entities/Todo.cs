﻿using System;

namespace Entities
{
    public class Todo
    {
        public string TodoText { get; set; }
        public DateTime TodoDate { get; set; }
        public Guid Id { get; set; }
    }
}