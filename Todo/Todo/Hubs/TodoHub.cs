﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ServiceInterfaces;
using Todo.Models;
using System.Linq;

namespace Todo.Hubs
{
    public class TodoHub : AuthenticatingHub
    {
        private readonly ITodoService _todoService;

        public TodoHub(ITodoService todoService)
        {
            _todoService = todoService;
        }

        public Task<List<TodoViewModel>> GetTodos()
        {
            return Task.Factory.StartNew(() =>
                    {
                        if (IsAuthenticated())
                        {
                            var todos = _todoService.GetTodos();
                            var model = todos.Select(t => new TodoViewModel{Id = t.Id, TodoDate = t.TodoDate, TodoText = t.TodoText}).ToList();
                            return model;
                        }
                        return null;
                    });
        }
    }
}