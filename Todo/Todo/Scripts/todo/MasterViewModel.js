﻿function MasterViewModel() {
    var self = this;

    self.todos = new ListModel(
     {
         cardinalityFormatter: function (count) {
             return (count == 1) ? resources.res('Todo.Resources.Resources.PostCardinalityOne') : count.toString() + " " + resources.res('Todo.Resources.Resources.PostCardinalityMany');
         }
     });

    self.todo = ko.observable(new TodoItemViewModel({ parent: self }));
    self.user = new UserModel();

    self.addTodoText = resources.res('Todo.Resources.Resources.AddTodoText');
    self.addTodo = function () {
        if (self.todo().todoText() != "" && Date.parse(self.todo().todoDate()) > (1).days().ago()) {
            Block();
            var todo = new TodoItemViewModel({ parent: self, data: self.todo });
            self.todos.push(todo);
            Unblock();
            return false;
        }
    };

    self.getTodos = function () {
        $.connection.todoHub.server.getTodos()
            .done(function (data) {
                if (data) {
                    $(data).each(function () {
                        var todo = new TodoItemViewModel({ parent: self, data: this });
                        self.todos.push(todo);
                    });
                }
            })
            .fail(function (error) {
                console.log(error);
            });
    };

    self.oninit = function () {
        $.connection.userHub.server.getUser()
            .done(function (data) {
                if (data) {
                    self.user.init(data);
                    self.getTodos();
                } else {
                    self.user.isAuthenticated(false);
                }
            })
            .fail(function (error) {
                console.log(error);
                self.user.isAuthenticated(false);
            }).always(function () {
                ko.applyBindings(self);
                //ko.applyBindings(page);
            });
    };
}
