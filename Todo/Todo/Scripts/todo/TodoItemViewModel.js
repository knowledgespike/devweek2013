﻿function TodoItemViewModel(options) {
    var self = this;
    var parent = options.parent;

    var listmodel = parent.todos;

    ListItemModel(this, listmodel);

    if (options.data != null) {
        if (typeof options.data != 'function') {
            self.id = options.data.Id;
            self.date = options.data.TodoDateTime == null ? null : new Date(Date.parse(options.data.TodoDate));
            self.todoDate = ko.observable(self.date);
            self.todoText = ko.observable(options.data.TodoText);
        } else {
            self.date = new Date(Date.parse(options.data().todoDate()));
            self.todoDate = ko.observable(self.date);
            self.todoText = ko.observable(options.data().todoText());
        }
    } else {
        self.id = 0;
        self.todoDate = ko.observable();
        self.todoText = ko.observable("");
    }

    self.displayDate = ko.computed(function () {
        if (self.todoDate() != null) {
            return self.todoDate().toString('dddd MMMM d, yyyy');
        } else {
            return "";
        }
    });
}
