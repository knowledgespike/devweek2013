﻿using System;

namespace Todo.Models
{
    public class TodoViewModel
    {
        public Guid Id { get; set; }
        public DateTime TodoDate { get; set; }
        public string TodoText { get; set; }
    }
}